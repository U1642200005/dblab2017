#initial time: 0.684 s
#after index: 0.563 s
#after creating PK: 0.123 s
SELECT * FROM uniprot_example.proteins;
select *
from proteins
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;
# this initial query was processed in 0.684 seconds
create index uniprot_index on proteins (uniprot_id);
select *
from proteins
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;
#after introduction of index, the process time is 0.563 seconds
drop index uniprot_index on proteins;
alter table proteins add constraint pk_proteins primary key (uniprot_id);
#when we run the same script after this step, yhe process time was 1.23 seconds.